#!/usr/bin/bash

docker-compose build
docker-compose up -d clickhouse logbroker
docker-compose run clickhouse-client -q 'create table kek (a Int32, b String) ENGINE = MergeTree() primary key a;'
docker-compose run curl logbroker:8000/show_create_table?table_name=kek
docker-compose run curl logbroker:8000/write_log -d "[{\"table_name\": \"kek\", \"rows\": [{\"a\":1, \"b\": \"some new row\"}], \"format\": \"json\"}]"
cat raw_binary.txt | docker-compose run curl logbroker:8000/write_log_bin?table_name=kek --data-binary @-
#docker-compose run curl logbroker:8000/status?query_id=77dba201-fab9-4611-bf31-641e971e1fe7
