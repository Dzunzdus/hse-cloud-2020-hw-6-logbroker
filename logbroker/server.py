import csv
import json
import os
import ssl
from io import StringIO
from collections import defaultdict
import aioschedule as schedule
import uuid 
import time
import threading
import asyncio
import dbm
import sys

from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientError
from fastapi import FastAPI, Request, Response

CH_HOST = os.getenv('LOGBROKER_CH_HOST', 'localhost')
CH_USER = os.getenv('LOGBROKER_CH_USER')
CH_PASSWORD = os.getenv('LOGBROKER_CH_PASSWORD')
CH_PORT = int(os.getenv('LOGBROKER_CH_PORT', 8123))
CH_CERT_PATH = os.getenv('LOGBROKER_CH_CERT_PATH')


def get_query_id():
    return str(uuid.uuid4())

def get_bytes_by_table(table_name, method):
    res = db_writes.get(json.dumps({'table_name': table_name, 'method': method}))
    if res is None:
        return {"id": get_query_id(), "rows": ""}
    try:
        loaded = json.loads(res)
    except Exception as e:
        print(str(e), file=sys.stderr, flush=True) 
    return loaded

def put_bytes_by_table(table_name, method, query_id, rows):
    db_writes[json.dumps({'table_name': table_name, 'method': method})] = json.dumps({'id': query_id, 'rows': rows})
    

class Cache:
    def __init__(self):
        pass 
    
    async def get_query_res(self, query_id):
        if query_id not in db_ids:
            return f"Query id {query_id} is not found\n"
        res = db_ids[query_id]
        return str(res) + "\n"
    
    async def update_raw_data(self, table_name, rows):
        query_with_id = get_bytes_by_table(table_name, 'raw')
        query_id = query_with_id['id']
        raw_bytes = bytes(query_with_id['rows'], 'utf-8') + rows
        put_bytes_by_table(table_name, 'raw', query_id, str(raw_bytes, encoding='utf-8'))
        return query_id
    
    async def update_each_row_data(self, table_name, rows):
        query_with_id = get_bytes_by_table(table_name, 'each_row')
        
        query_id = query_with_id['id']
        old_rows = query_with_id['rows']
        stream = StringIO(old_rows)
        for row in rows:
            assert isinstance(row, dict)
            stream.write(json.dumps(row))
            stream.write('\n')
        
        put_bytes_by_table(table_name, 'each_row', query_id, stream.getvalue())
        return query_id
    
    async def update_csv_data(self, table_name, rows):
        query_with_id = get_bytes_by_table(table_name, 'csv')
        query_id = query_with_id['id']
        old_rows = query_with_id['rows']
        stream = StringIO(old_rows)
        
        cwr = csv.writer(stream, quoting=csv.QUOTE_ALL)
        cwr.writerows(rows)
        put_bytes_by_table(table_name, 'csv', query_id, stream.getvalue())
        return query_id
    
    async def dump_cache(self):
        print("Dumping cache ...", file=sys.stderr, flush=True)
        query_keys = db_writes.keys()
        
        for key in query_keys:
            value = db_writes[key]
            key_json = json.loads(key)
            value_json = json.loads(value)
            table_name = key_json['table_name']
            method = key_json['method']
            query_id = value_json['id']
            res = None
            if (method == 'each_row') or (method == 'csv'):
                data = StringIO(value_json['rows'])
                data.seek(0)
                if method == 'each_row':
                    res = await query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT JSONEachRow', data)
                elif method == 'csv':
                    res = await query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT CSV', data)
                else:
                    print(f"Got unsupported method '{method}', {query_id}")
            elif method == 'raw':
                data = bytes(value_json['rows'], 'utf-8')
                res = await query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT RowBinary', data)
            else:
                print(f"Got unsupported method '{method}', {query_id}")
    
            print(f"Executed query '{query_id}'", file=sys.stderr, flush=True)
            db_ids[query_id] = json.dumps(res)
            del db_writes[key]
                
        self.__init__()
    

async def execute_query(query, data=None):
    url = f'http://{CH_HOST}:{CH_PORT}/'
    params = {
        'query': query.strip()
    }
    headers = {}
    if CH_USER is not None:
        headers['X-ClickHouse-User'] = CH_USER
        if CH_PASSWORD is not None:
            headers['X-ClickHouse-Key'] = CH_PASSWORD
    ssl_context = ssl.create_default_context(cafile=CH_CERT_PATH) if CH_CERT_PATH is not None else None

    async with ClientSession() as session:
        async with session.post(url, params=params, data=data, headers=headers, ssl=ssl_context) as resp:
            await resp.read()
            try:
                resp.raise_for_status()
                return resp, None
            except ClientError as e:
                return resp, {'error': str(e)}

async def dump_cache(cache):
    print("Dump cache!", file=sys.stderr, flush=True)
    while True:
        await asyncio.sleep(5)
        await cache.dump_cache()

cache = Cache()
app = FastAPI()

db_writes = dbm.open('writes', 'c')
db_ids = dbm.open('ids', 'c')

async def query_wrapper(query, data=None):
    print(f"Execute query: {query}", file=sys.stderr, flush=True)
    res, err = await execute_query(query, data)
    if err is not None:
        return err
    return await res.text()


@app.get('/show_create_table')
async def show_create_table(table_name: str):
    resp = await query_wrapper(f'SHOW CREATE TABLE "{table_name}";')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp


async def send_csv(table_name, rows):
    return await cache.update_csv_data(table_name, rows)
    

async def send_json_each_row(table_name, rows):
    return await cache.update_each_row_data(table_name, rows)

async def send_raw(table_name, rows):
    return await cache.update_raw_data(table_name, rows)


@app.post('/write_log')
async def write_log(request: Request):  
    body = await request.json()
    res = []
    for log_entry in body:
        table_name = log_entry['table_name']
        rows = log_entry['rows']
        if log_entry.get('format') == 'list':
            res.append(await send_csv(table_name, rows))
        elif log_entry.get('format') == 'json':
            res.append(await send_json_each_row(table_name, rows))
        else:
            res.append({'error': f'unknown format {log_entry.get("format")}, you must use list or json'})
    return res


@app.post('/write_log_bin')
async def write_log_bin(table_name: str, request: Request):
    body = await request.body()
    return await send_raw(table_name, body)

@app.get('/status')
async def status(query_id: str):
    return await cache.get_query_res(query_id)

@app.get('/healthcheck')
async def healthcheck():
    return Response(content='Ok', media_type='text/plain')


@app.on_event("startup")
async def startup_event():
    asyncio.create_task(dump_cache(cache))