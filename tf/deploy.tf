terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
            version = "0.49.0"
        }
    }
}

provider "yandex" {
    token = "AgAAAAAds8u3AATuwUi8rq-LK0m4oumwr3lPMeM"
    cloud_id = "b1gao5nvuklp7p1a4p2u"
    folder_id = "b1gisah0e9q5lk2on664"
    zone = "ru-central1-a"
}

resource "yandex_compute_instance_group" "backend" {
    name = "backend"
    service_account_id = "aje9qjc79tgjkfllatll"
    instance_template {
        name = "backend-{instance.index}"
        platform_id = "standard-v2"
        resources {
            memory = 2
            cores = 2
        }
        boot_disk {
            mode = "READ_WRITE"
            initialize_params {
                image_id = data.yandex_compute_image.container-optimized-image.id
            }
        }
        network_interface {
            network_id = yandex_vpc_network.vpc-net.id
            subnet_ids = [yandex_vpc_subnet.private-subnet.id]
            nat = false
        }
        metadata = {
            ssh-keys = "altruist:${file("~/.ssh/id_ed25519.pub")}"
            docker-compose = file("./backend-compose.yaml")
        }
        service_account_id = "aje9qjc79tgjkfllatll"
        scheduling_policy {
            preemptible = true
        }
    }
    scale_policy {
        auto_scale {
            initial_size = 2
            measurement_duration = 60
            cpu_utilization_target = 10
            max_size = 10
            warmup_duration = 5
            stabilization_duration = 60
            min_zone_size = 1
        }
    }
    allocation_policy {
        zones = [
            "ru-central1-a"]
    }
    load_balancer {
      target_group_name = "backend"
    }
    deploy_policy {
        max_unavailable = 1
        max_expansion = 2
    }
}

resource "yandex_compute_instance" "nginx" {
    name = "nginx"
    hostname = "nginx"
    platform_id = "standard-v2"
    service_account_id = "aje9qjc79tgjkfllatll"
    resources {
        cores  = 2
        memory = 2
    }

    boot_disk {
        initialize_params {
            image_id = data.yandex_compute_image.container-optimized-image.id
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.public-subnet.id
        nat       = true
    }

    metadata = {
        ssh-keys = "altruist:${file("~/.ssh/id_rsa.pub")}"
    }
}

resource "yandex_lb_network_load_balancer" "balancer" {
  name = "load-balancer"

  listener {
    name = "listener"
    port = 8080
    target_port = 8000
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.backend.load_balancer.0.target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 8000
        path = "/healthcheck"
      }
    }
  }
}

resource "yandex_compute_instance" "clickhouse" {
    name = "clickhouse"
    hostname = "clickhouse"
    platform_id = "standard-v2"
    service_account_id = "aje9qjc79tgjkfllatll"
    resources {
        cores  = 2
        memory = 2
    }

    boot_disk {
        initialize_params {
            image_id = data.yandex_compute_image.container-optimized-image.id
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.public-subnet.id
        nat       = true
    }

    metadata = {
        docker-compose = file("./ch-compose.yaml")
        ssh-keys = "altruist:${file("~/.ssh/id_rsa.pub")}"
    }
}

data "yandex_compute_image" "container-optimized-image" {
    family = "container-optimized-image"
}